<?php
/**
 * Widget show Doc Comment
 *
 * @category Class
 * @package  MyPackage
 * @author   Beno <benisan150@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */

/**
* This function is the constructor of ControllerAccountChangePayment class
*
* @return json
*/
function request()
{
    $url = "https://test.oppwa.com/v1/checkouts";
    $data = "authentication.userId=8a8294174b7ecb28014b9699220015cc" .
        "&authentication.password=sy6KJsT8" .
        "&authentication.entityId=8a8294174b7ecb28014b9699220015ca" .
        "&amount=92.00" .
        "&currency=EUR" .
        "&paymentType=DB".
        "&customParameters[order_id]=543251321";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if (curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    return json_decode($responseData);
}
$responseData =request();
echo "<pre>", print_r($responseData->id, 1), "</pre>";
?>

<script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php echo $responseData->id?>"></script>

<form action="http://localhost/test_result/2.php" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>
